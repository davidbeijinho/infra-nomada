---
# Source: authelia/templates/configMap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: authelia-hive
  labels: 
    app.kubernetes.io/name: authelia
    app.kubernetes.io/instance: authelia-hive
    app.kubernetes.io/version: 4.38.16
    app.kubernetes.io/managed-by: Helm
    helm.sh/chart: authelia-0.9.16
data:
  configuration.yaml: |
    ---
    # yaml-language-server: $schema=https://www.authelia.com/schemas/v4.38/json-schema/configuration.json
    theme: 'dark'
    default_2fa_method: ''
    server:
      address: 'tcp://0.0.0.0:9091/'
      asset_path: ''
      headers:
        csp_template: ''
      buffers:
        read: 4096
        write: 4096
      timeouts:
        read: '6 seconds'
        write: '6 seconds'
        idle: '30 seconds'
      endpoints:
        enable_pprof: false
        enable_expvars: false
        authz:
          auth-request:
            implementation: 'AuthRequest'
          ext-authz:
            implementation: 'ExtAuthz'
          forward-auth:
            implementation: 'ForwardAuth'
    log:
      level: 'info'
      format: 'text'
      file_path: ''
      keep_stdout: true
    telemetry:
      metrics:
        enabled: false
    identity_validation:
      elevated_session:
        code_lifespan: '5 minutes'
        elevation_lifespan: '10 minutes'
        characters: 8
        require_second_factor: false
        skip_second_factor: false
      reset_password:
        jwt_lifespan: '5 minutes'
        jwt_algorithm: 'HS256'
    totp:
      disable: false
      issuer: 'Authelia'
      skew: 1
      secret_size: 32
      algorithm: 'SHA1'
      digits: 6
      period: 30
      allowed_algorithms:
        - 'SHA1'
      allowed_digits:
        - 6
      allowed_periods:
        - 30
    webauthn:
      disable: false
      display_name: 'Authelia'
      attestation_conveyance_preference: 'indirect'
      user_verification: 'preferred'
      timeout: '60 seconds'
    ntp:
      address: 'udp://time.cloudflare.com:123'
      version: 4
      max_desync: '3 seconds'
      disable_startup_check: false
      disable_failure: false
    authentication_backend:
      password_reset:
        disable: true
        custom_url: ''
      file:
        path: '/config-users/users_database.yml'
        watch: false
        search:
          email: false
          case_insensitive: false
        password:
          algorithm: 'argon2'
          argon2:
            variant: 'argon2id'
            iterations: 3
            memory: 65536
            parallelism: 4
            key_length: 32
            salt_length: 16
          scrypt:
            iterations: 16
            block_size: 8
            parallelism: 1
            key_length: 32
            salt_length: 16
          pbkdf2:
            variant: 'sha512'
            iterations: 310000
            salt_length: 16
          sha2crypt:
            variant: 'sha512'
            iterations: 50000
            salt_length: 16
          bcrypt:
            variant: 'standard'
            cost: 12
    password_policy:
      standard:
        enabled: false
        min_length: 8
        max_length: 0
        require_uppercase: false
        require_lowercase: false
        require_number: false
        require_special: false
      zxcvbn:
        enabled: false
        min_score: 0
    session:
      name: 'authelia_session'
      same_site: 'lax'
      inactivity: '24h'
      expiration: '24h'
      remember_me: '1 month'
      cookies:
        - domain: 'hive.thebeijinho.com'
          authelia_url: 'https://auth-hive.hive.thebeijinho.com'
          default_redirection_url: 'https://whoami.hive.thebeijinho.com'
    regulation:
      max_retries: 3
      find_time: '2 minutes'
      ban_time: '5 minutes'
    storage:
      local:
        path: '/config/db.sqlite3'
    notifier:
      disable_startup_check: false
      filesystem:
        filename: '/config/notification.txt'
    access_control:
      default_policy: 'deny'
      rules:
        - policy: bypass
          domain:
            - 'silver.hive.thebeijinho.com'
          resources:
            - '/.client/manifest.json$'
            - '/.client/[a-zA-Z0-9_-]+.png$'
            - '/service_worker.js$'
        - policy: bypass
          domain:        
            - 'speedtest.hive.thebeijinho.com'
            - 'grafana.hive.thebeijinho.com'
            - 'home-assistant.hive.thebeijinho.com'
            - 'jellyfin.hive.thebeijinho.com'
            - 'nextcloud.hive.thebeijinho.com'
            - 'photoprism.hive.thebeijinho.com'
            - 'samid.hive.thebeijinho.com'
            - 'whoami.hive.thebeijinho.com'
            - 'auth-hive.hive.thebeijinho.com'
            - 'urls.hive.thebeijinho.com'
            - 'argo-events-gitlab.hive.thebeijinho.com'
            - 'audiobookshelf.hive.thebeijinho.com'
            - 'wallabag.hive.thebeijinho.com'
            - 'files.hive.thebeijinho.com'
            - 'pocketbase.hive.thebeijinho.com'
            - 'grist.hive.thebeijinho.com'
            - 'authentik.hive.thebeijinho.com'
            - 'sogo.hive.thebeijinho.com'
            - 'mediawiki.hive.thebeijinho.com'
        - policy: one_factor
          domain:        
            - 'smokeping.hive.thebeijinho.com'
            - 'trilium.hive.thebeijinho.com'
            - 'brain.hive.thebeijinho.com'
            - 'pots-diary.hive.thebeijinho.com'
            - 'notes.hive.thebeijinho.com'
            - 'argocd.hive.thebeijinho.com'
            - 'argo-workflows.hive.thebeijinho.com'
            - 'hive.thebeijinho.com'
            - 'filebrowser.hive.thebeijinho.com'
            - 'hive.thebeijinho.com'
            - 'traefik.hive.thebeijinho.com'
            - 'nodered.hive.thebeijinho.com'
            - 'shlink.hive.thebeijinho.com'
            - 'syncthing.hive.thebeijinho.com'
            - 'adminer.hive.thebeijinho.com'
            - 'wiki.hive.thebeijinho.com'
            - 'habits.hive.thebeijinho.com'
            - 'silver.hive.thebeijinho.com'
            - 'navidrome.hive.thebeijinho.com'
            - 'coder.hive.thebeijinho.com'
          subject:
            - ['group:admins']
    ...
---
# Source: authelia/templates/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: authelia-hive
  labels: 
    app.kubernetes.io/name: authelia
    app.kubernetes.io/instance: authelia-hive
    app.kubernetes.io/version: 4.38.16
    app.kubernetes.io/managed-by: Helm
    helm.sh/chart: authelia-0.9.16
spec:
  type: ClusterIP
  sessionAffinity: None
  selector:
    app.kubernetes.io/name: authelia
    app.kubernetes.io/instance: authelia-hive
  ports:
    - name: http
      protocol: TCP
      port: 80
      targetPort: http
---
# Source: authelia/templates/deployment.yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: authelia-hive
  labels: 
    app.kubernetes.io/name: authelia
    app.kubernetes.io/instance: authelia-hive
    app.kubernetes.io/version: 4.38.16
    app.kubernetes.io/managed-by: Helm
    helm.sh/chart: authelia-0.9.16
spec:
  selector:
    matchLabels: 
      app.kubernetes.io/name: authelia
      app.kubernetes.io/instance: authelia-hive
  revisionHistoryLimit: 1
  minReadySeconds: 0
  updateStrategy:
    type: RollingUpdate
    rollingUpdate: 
      maxUnavailable: 25%
  template:
    metadata:
      labels: 
        app.kubernetes.io/name: authelia
        app.kubernetes.io/instance: authelia-hive
        app.kubernetes.io/version: 4.38.16
        app.kubernetes.io/managed-by: Helm
        helm.sh/chart: authelia-0.9.16
        
    spec:
      hostNetwork: false
      hostPID: false
      hostIPC: false
      affinity: 
        nodeAffinity: {}
        podAffinity: {}
        podAntiAffinity: {}
      enableServiceLinks: false
      containers:
      - name: authelia
        image: ghcr.io/authelia/authelia:4.38.16
        imagePullPolicy: IfNotPresent
        command:
        - 'authelia'
        resources: 
          limits: {}
          requests: {}
        env:
        - name: AUTHELIA_SERVER_DISABLE_HEALTHCHECK
          value: "true"
        - name: AUTHELIA_IDENTITY_VALIDATION_RESET_PASSWORD_JWT_SECRET_FILE
          value: '/secrets/internal/JWT_TOKEN'
        - name: AUTHELIA_SESSION_SECRET_FILE
          value: '/secrets/internal/SESSION_ENCRYPTION_KEY'
        - name: AUTHELIA_STORAGE_ENCRYPTION_KEY_FILE
          value: '/secrets/internal/STORAGE_ENCRYPTION_KEY'
        - name: X_AUTHELIA_CONFIG
          value: '/configuration.yaml'
        - name: X_AUTHELIA_CONFIG_FILTERS
          value: template
        startupProbe:
          failureThreshold: 6
          httpGet:
            path: /api/health
            port: http
            scheme: HTTP
          initialDelaySeconds: 10
          periodSeconds: 5
          successThreshold: 1
          timeoutSeconds: 5
        livenessProbe:
          failureThreshold: 5
          httpGet:
            path: /api/health
            port: http
            scheme: HTTP
          initialDelaySeconds: 0
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 5
        readinessProbe:
          failureThreshold: 5
          httpGet:
            path: /api/health
            port: http
            scheme: HTTP
          initialDelaySeconds: 0
          periodSeconds: 5
          successThreshold: 1
          timeoutSeconds: 5
        ports:
        - name: http
          containerPort: 9091
          protocol: TCP
        volumeMounts:
        - mountPath: /configuration.yaml
          name: config
          readOnly: true
          subPath: configuration.yaml
        - mountPath: /secrets/internal
          name: secrets
          readOnly: true
        - mountPath: /config-users
          name: user-database
      volumes:
      - name: config
        configMap:
          name: authelia-hive
          items:
          - key: configuration.yaml
            path: configuration.yaml
      - name: secrets
        secret:
          secretName: authelia-tokens
          items:
          - key: JWT_TOKEN
            path: JWT_TOKEN
          - key: SESSION_ENCRYPTION_KEY
            path: SESSION_ENCRYPTION_KEY
          - key: STORAGE_ENCRYPTION_KEY
            path: STORAGE_ENCRYPTION_KEY
      - name: user-database
        secret:
          items:
          - key: user-database.yaml
            path: users_database.yml
          secretName: authelia-database
---
# Source: authelia/templates/traefikCRD/ingressRoute.yaml
apiVersion: traefik.io/v1alpha1
kind: IngressRoute
metadata:
  name: authelia-hive
  labels: 
    app.kubernetes.io/name: authelia
    app.kubernetes.io/instance: authelia-hive
    app.kubernetes.io/version: 4.38.16
    app.kubernetes.io/managed-by: Helm
    helm.sh/chart: authelia-0.9.16
spec:
  entryPoints:
  - web
  - websecure
  routes:
  - kind: Rule
    match: Host(`auth-hive.hive.thebeijinho.com`) && PathPrefix(`/`)
    priority: 10
    middlewares:
      - name: chain-authelia-hive
        namespace: auth-hive
    services:
      - kind: Service
        name: authelia-hive
        port: 80
        namespace: auth-hive
        passHostHeader: true
        strategy: RoundRobin
        scheme: http
        weight: 10
        responseForwarding:
          flushInterval: 100ms
---
# Source: authelia/templates/traefikCRD/middlewares.yaml
apiVersion: traefik.io/v1alpha1
kind: Middleware
metadata:
  name: forwardauth-authelia-hive
  labels: 
    app.kubernetes.io/name: authelia
    app.kubernetes.io/instance: authelia-hive
    app.kubernetes.io/version: 4.38.16
    app.kubernetes.io/managed-by: Helm
    helm.sh/chart: authelia-0.9.16
spec:
  forwardAuth:
    address: 'http://authelia-hive.auth-hive.svc.cluster.local/api/authz/forward-auth'
    trustForwardHeader: true
    authResponseHeaders:
    - 'Remote-User'
    - 'Remote-Name'
    - 'Remote-Email'
    - 'Remote-Groups'
---
# Source: authelia/templates/traefikCRD/middlewares.yaml
apiVersion: traefik.io/v1alpha1
kind: Middleware
metadata:
  name: chain-authelia-hive-auth
  labels: 
    app.kubernetes.io/name: authelia
    app.kubernetes.io/instance: authelia-hive
    app.kubernetes.io/version: 4.38.16
    app.kubernetes.io/managed-by: Helm
    helm.sh/chart: authelia-0.9.16
spec:
  chain:
    middlewares:
      - name: forwardauth-authelia-hive
        namespace: auth-hive
---
# Source: authelia/templates/traefikCRD/middlewares.yaml
apiVersion: traefik.io/v1alpha1
kind: Middleware
metadata:
  name: headers-authelia-hive
  labels: 
    app.kubernetes.io/name: authelia
    app.kubernetes.io/instance: authelia-hive
    app.kubernetes.io/version: 4.38.16
    app.kubernetes.io/managed-by: Helm
    helm.sh/chart: authelia-0.9.16
spec:
  headers:
    browserXssFilter: true
    customFrameOptionsValue: "SAMEORIGIN"
    customResponseHeaders:
      Cache-Control: "no-store"
      Pragma: "no-cache"
---
# Source: authelia/templates/traefikCRD/middlewares.yaml
apiVersion: traefik.io/v1alpha1
kind: Middleware
metadata:
  name: chain-authelia-hive
  labels:
    app.kubernetes.io/name: authelia
    app.kubernetes.io/instance: authelia-hive
    app.kubernetes.io/version: 4.38.16
    app.kubernetes.io/managed-by: Helm
    helm.sh/chart: authelia-0.9.16
spec:
  chain:
    middlewares:
      - name: headers-authelia-hive
        namespace: auth-hive
