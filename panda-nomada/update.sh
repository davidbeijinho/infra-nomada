#!/bin/sh

real_path=$(realpath "$0")
dir_path=$(dirname "$real_path")
kube_version=v1.25.3+k3s1

kubectl config use-context panda-nomada


### argo-cd
helm template argocd argo/argo-cd --debug --dry-run --include-crds --namespace argocd --kube-version=$kube_version --values="$dir_path/resources/argo-cd/values.yaml" >"$dir_path/resources/argo-cd/resources-helm.yaml"
### authelia-hive
helm template authelia-hive authelia/authelia --debug --dry-run --include-crds --namespace auth-hive --kube-version=$kube_version --values="$dir_path/resources/authelia-hive/values.yaml" >"$dir_path/resources/authelia-hive/resources-helm.yaml"
### authelia-panda
helm template authelia-panda authelia/authelia --debug --dry-run --include-crds --namespace auth-panda --kube-version=$kube_version --values="$dir_path/resources/authelia-panda/values.yaml" >"$dir_path/resources/authelia-panda/resources-helm.yaml"
### cert-manager
helm template cert-manager jetstack/cert-manager --debug --dry-run --include-crds --namespace cert-manager --kube-version=$kube_version --values="$dir_path/resources/cert-manager/values.yaml" >"$dir_path/resources/cert-manager/resources-helm.yaml"
### cloudflare-dns
### filebrowser
### gitops
### local-path-storage
### plausible
# helm template plausible-analytics imio/plausible-analytics --debug --dry-run --include-crds --namespace plausible-analytics --kube-version=$kube_version --values="$dir_path/resources/plausible-analytics/values.yaml" >"$dir_path/resources/plausible-analytics/resources-helm.yaml"
helm template plausible-analytics zekker6/plausible-analytics --debug --dry-run --include-crds --namespace plausible-analytics --kube-version=$kube_version --values="$dir_path/resources/plausible-analytics/values.yaml" >"$dir_path/resources/plausible-analytics/resources-helm.yaml"
### rathole
### sealed-secrets
helm template sealed-secrets sealed-secrets/sealed-secrets --debug --dry-run --include-crds --namespace kube-system --kube-version=$kube_version --values="$dir_path/resources/sealed-secrets/values.yaml" >"$dir_path/resources/sealed-secrets/resources-helm.yaml"
### syncthing
### traefik
helm template traefik traefik/traefik --debug --dry-run --include-crds --namespace default --kube-version=$kube_version --values="$dir_path/resources/traefik/values.yaml" >"$dir_path/resources/traefik/resources-helm.yaml"
### whoami
### wireguard

# ARCHIVE

# helm template blog bitnami/wordpress --debug --dry-run --include-crds --namespace blog --kube-version=$kube_version --values="$dir_path/resources/blog/values.yaml" >"$dir_path/resources/blog/resources-helm.yaml"
# helm template wordpress bitnami/wordpress --debug --dry-run --include-crds --namespace wordpress --kube-version=$kube_version --values="$dir_path/resources/wordpress/values.yaml" >"$dir_path/resources/wordpress/resources-helm.yaml"
# helm template nextcloud nextcloud/nextcloud --dry-run --include-crds --namespace auth --debug --kube-version=$kube_version --values="$dir_path/resources/nextcloud/values.yaml" >"$dir_path/resources/nextcloud/resources-helm.yaml"
# helm template docker-mailserver docker-mailserver/docker-mailserver --dry-run --include-crds --namespace docker-mailserver --debug --kube-version=$kube_version --values="$dir_path/resources/docker-mailserver/values.yaml" >"$dir_path/resources/docker-mailserver/resources-helm.yaml"
# helm template teleport teleport/teleport-cluster                  --debug --dry-run --include-crds --namespace teleport      --kube-version=$kube_version --values="$dir_path/resources/teleport/values.yaml" >"$dir_path/resources/teleport/resources-helm.yaml"
