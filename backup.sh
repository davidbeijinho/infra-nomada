#!/bin/sh

time_str=$(date +"%F_%T")

# kubectl get --all-namespaces -o yaml namespaces,cert > ./backups/backup_other_"$time_str".yaml 

# kubectl get secrets -A -o jsonpath='{.items[?(@.metadata.annotations.cert-manager\.io\/issuer-name=="cloudflare-dns-production")].metadata.name}' -o yaml > ./backups/backup_secrets_"$time_str".yaml 

# kubectl get pvc -A -o jsonpath='{.items[?(@.metadata.annotations.volume.kubernetes.io/storage-provisioner=="driver.longhorn.io")].metadata.name}' -o yaml > ./backups/backup_pvc_"$time_str".yaml 
kubectl get pvc -A -o=jsonpath='{.items[?(@.spec.storageClassName=="longhorn-sc-config")]}' > ./backups/backup_pvc_"$time_str".json 

# kubectl get PersistentVolume -A -o jsonpath='{.items[?(@.metadata.annotations.pv.kubernetes.io/provisioned-by=="driver.longhorn.io")].metadata.name}' -o yaml > ./backups/backup_pv_"$time_str".yaml 
