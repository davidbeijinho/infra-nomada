#!/bin/sh

#smokeping
helm repo add djjudas21 https://djjudas21.github.io/charts/
#sogo
helm repo add sogo https://helm.snry.xyz/docker-sogo/
#ntfy
helm repo add fmjstudios https://fmjstudios.github.io/helm
# mariadb nginx redis
helm repo add bitnami https://charts.bitnami.com/bitnami
# helm repo add traefik https://helm.traefik.io/traefik
helm repo add traefik https://traefik.github.io/charts
helm repo add sealed-secrets https://bitnami-labs.github.io/sealed-secrets
# cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo add grafana https://grafana.github.io/helm-charts
helm repo add authelia https://charts.authelia.com
# filebrowser homer
helm repo add k8s-at-home https://k8s-at-home.com/charts
helm repo add nextcloud https://nextcloud.github.io/helm
helm repo add argo https://argoproj.github.io/argo-helm
helm repo add renovate https://docs.renovatebot.com/helm-charts
# pgadmin4
helm repo add runix https://helm.runix.net
# shlink-web shlink-back
# helm repo add christianknell https://christianknell.github.io/helm-charts
# helm repo add christianhuth https://charts.christianhuth.de
# shlink
nhuth.de
# prometheus smart
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
# authentik
helm repo add goauthentik https://charts.goauthentik.io/
# code-server
helm repo add deploy-code-server https://code-server-boilerplates.github.io/charts
# kestra
helm repo add kestra https://helm.kestra.io/
# trilium
helm repo add trilium https://triliumnext.github.io/helm-charts
# external-secrets
helm repo add external-secrets-operator https://charts.external-secrets.io/
# netdata
helm repo add netdata https://netdata.github.io/helmchart
# local-ai
helm repo add localai https://go-skynet.github.io/helm-charts/
helm repo add awx-operator https://ansible-community.github.io/awx-operator-helm/
helm repo add coder-v2 https://helm.coder.com/v2
helm repo add minio https://charts.min.io/
helm repo add bitwarden https://charts.bitwarden.com/
#helm install ansible-semaphore oci://ghcr.io/cloudhippie/charts/ansible-semaphore
# ARCHIVE
# helm repo add budibase https://budibase.github.io/budibase
# helm repo add windmill https://windmill-labs.github.io/windmill-helm-charts
# helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard
#helm repo add ebrianne.github.io https://ebrianne.github.io/helm-charts
# helm repo add docker-mailserver https://docker-mailserver.github.io/docker-mailserver-helm
# helm repo add longhorn https://charts.longhorn.io
# helm repo add truecharts https://charts.truecharts.org
# helm repo add ittrident https://ittrident.github.io/helm-charts
# helm repo add gitea-charts https://dl.gitea.io/charts/
# helm repo add drone https://charts.drone.io
# helm repo add p80n https://p80n.github.io/photoprism-helm
# helm repo add halkeye https://halkeye.github.io/helm-charts
# helm repo add lib42 https://lib42.github.io/charts
# helm repo add awx-operator https://ansible.github.io/awx-operator
# helm repo add minio https://helm.min.io
# helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner
# helm repo add vmware-tanzu https://vmware-tanzu.github.io/helm-charts
# helm repo add geek-cookbook https://geek-cookbook.github.io/charts