# Kubectl

## Kubectl Contexts

### Display the current Context
```bash
kubectl config current-context
```

### List all the Contexts in a kubeconfig file
```bash
kubectl config get-contexts
```

### Switch Context
```bash
kubectl config use-context NAME
```
