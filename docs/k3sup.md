# K3sUp

## Instal master
```bash
k3sup install --ip IP --user USER --context CONTEXT --local-path ~/.kube/config --merge --ssh-key ~/.ssh/id_ed25519 --print-command --k3s-version stable--k3s-extra-args '--no-deploy traefik'
```

## Instal master command
```bash
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC='server --tls-san IP' INSTALL_K3S_CHANNEL='v1.19' sh -
```

## Install worker
```bash
k3sup join --ip IP --server-ip IP --user root --ssh-key ~/.ssh/id_ed25519 --print-command
```

## Install worker command
```bash
curl -sfL https://get.k3s.io | K3S_URL='https://IP:6443' K3S_TOKEN='TOKEN' INSTALL_K3S_CHANNEL='v1.19' sh -s -
```
