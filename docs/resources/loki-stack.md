# Loki-stack

## Update helm files
```bash
helm template loki-stack grafana/loki-stack --dry-run --include-crds --namespace loki-stack --debug --values=./values.yaml > ./resources-helm.yaml
```
