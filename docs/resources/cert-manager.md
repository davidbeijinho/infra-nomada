# Cert-manager

## Update helm files
```bash
helm template cert-manager jetstack/cert-manager --dry-run --include-crds --namespace cert-manager --debug --values=./values.yaml > ./resources-helm.yaml
```

## Fix tls options
remove extra tls options in the ingress route