# NextCloud

## Update helm files
```bash
helm template nextcloud nextcloud/nextcloud --dry-run --include-crds --namespace nextcloud --debug --values=./values.yaml > ./resources-helm.yaml
```
