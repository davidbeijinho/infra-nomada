# NFS share

## Mount NFS share
```bash
sudo mount -t nfs -o vers=4 nfs.pinactico.thebeijinho.com:/ /home/nomada/NFS_SHARE
```
