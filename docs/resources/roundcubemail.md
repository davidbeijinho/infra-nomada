# GUIDE

FIRST INSTALL

/usr/bin/composer require roundcube/carddav --update-no-dev
import the sql, removing the TABLE_PREFIX
/usr/bin/composer require "texxasrulez/calendar:dev-master" --update-no-dev

edit /var/www/html/plugins/calendar/config.inc.php
and remove the key calendar_preinstalled_calendars


## attach to container and run

```bash
/usr/bin/composer require johndoh/contextmenu --update-no-dev
/usr/bin/composer require kolab/calendar --update-no-dev
/usr/bin/composer require roundcube/carddav --update-no-dev

/usr/bin/composer require  sabre/dav --update-no-dev

/usr/bin/composer require "texxasrulez/calendar:master" --update-no-dev --ignore-platform-reqs

/usr/bin/composer require  https://github.com/texxasrulez/calendar --update-no-dev

/usr/bin/composer require "texxasrulez/calendar:~0.0.7.0" --update-no-dev
```

### OTHER APROACH

```bash
/usr/bin/composer require roundcube/carddav --update-no-dev

/usr/bin/composer require "texxasrulez/calendar:dev-master" --update-no-dev
```

edit /var/www/html/plugins/calendar/config.inc.php
and remove the key calendar_preinstalled_calendars