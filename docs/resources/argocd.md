# ARGO CD

## Added insecure flag
- <https://github.com/argoproj/argo-cd/issues/2953>

## Helm chart
- NOT WORKING
```bash
helm template argo argo/argo-cd --dry-run --include-crds --debug --values=./values.yaml > ./resources-helm.yaml
```

## Password reset
- <https://argo-cd.readthedocs.io/en/stable/faq/#i-forgot-the-admin-password-how-do-i-reset-it>
- <https://www.browserling.com/tools/bcrypt>
