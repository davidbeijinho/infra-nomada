# Loki

## Update helm files
```bash
helm template loki grafana/loki --dry-run --include-crds --namespace loki --debug --values=./values.yaml > ./resources-helm.yaml
```
