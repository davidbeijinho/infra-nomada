# Authelia

## Update helm files
```bash
helm template authelia authelia/authelia --dry-run --include-crds --namespace auth --debug --values=./values.yaml > ./resources-helm.yaml
```

## Fix tls options
remove extra tls options in the ingress route


## passwords docs
<https://www.authelia.com/docs/configuration/authentication/file.html#passwords>

## create hash
```bash
docker run authelia/authelia:latest authelia hash-password 'PASSWORD'
```
