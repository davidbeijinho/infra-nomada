#!/bin/sh

kubectl create secret generic authelia-database --dry-run=client --namespace auth --from-file=user-database.yaml=./users_database.yml -o yaml >temp-panda.yaml

kubectl config use-context panda-nomada

kubeseal <temp-panda.yaml >users-panda.yaml --controller-name=sealed-secrets -o yaml -n auth

rm temp-panda.yaml
