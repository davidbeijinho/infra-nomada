#!/bin/sh

kubectl create secret generic authelia-database --dry-run=client --namespace auth --from-file=user-database.yaml=./users_database.yml -o yaml >temp-mini.yaml

kubectl config use-context mini-nomada

kubeseal <temp-mini.yaml >users-mini.yaml --controller-name=sealed-secrets -o yaml -n auth

rm temp-mini.yaml
