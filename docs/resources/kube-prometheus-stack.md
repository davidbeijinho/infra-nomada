# kube prometheus stack

## Update helm files
```bash
helm template prometheus prometheus-community/kube-prometheus-stack --dry-run --include-crds --namespace prometheus --debug --values=./values.yaml > ./resources-helm.yaml
```
