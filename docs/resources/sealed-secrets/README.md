# Sealed secrets

## Update helm files
```bash
    helm template sealed-secrets sealed-secrets/sealed-secrets  --dry-run --include-crds --namespace kube-system --debug --values=./values.yaml > ./resources-helm.yaml
```

---

## Create a temporary secret
```bash
echo -n SECRET_VALUE | kubectl create secret generic SECRET_NAME --dry-run=client --namespace NAMESPACE --from-file=KEY_NAME=/dev/stdin -o yaml >mysecret.yaml 
```
after add namespace to `mysecret.yaml`
change KEY_NAME name and SECRET_NAME

## seal the secret
```bash
kubeseal <mysecret.yaml >mysealedsecret.yaml --controller-name=sealed-secrets -o yaml
```

## seal the secret into namespace
```bash
kubeseal <mysecret.yaml >mynamespacedsealedsecret.yaml --controller-name=sealed-secrets -o yaml -n NAMESPACE
```
mysealedsecret.json is safe to upload to github, post to twitter, etc.

## Download the certeficate
```bash
kubeseal --controller-name=sealed-secrets --fetch-cert >mycert.pem 
```
for offilne sealing of without access to the cluster

## Backup the keys
```bash
kubectl get secret -n kube-system -l sealedsecrets.bitnami.com/sealed-secrets-key -o yaml >master.yaml
```
for backup to restore after disaster, keep it safe

## recovery unseal
```bash
kubeseal --recovery-unseal --recovery-private-key master.yaml <mysealedsecret.yaml >myunsealedsecret.yaml
```

## Restore the keys
```bash
kubectl apply -f master.key
```
### Delete current controler
```bash
kubectl delete pod -n kube-system -l name=sealed-secrets-controller
```
not working???
