# Fluent-bit

## Update helm files
```bash
helm template fluent-bit fluent/fluent-bit --dry-run --include-crds --namespace fluent-bit --debug --values=./values.yaml > ./resources-helm.yaml
```
