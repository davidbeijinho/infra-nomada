# Grafana

## Update helm files
```bash
helm template grafana grafana/grafana --dry-run --include-crds --namespace grafana --debug --values=./values.yaml > ./resources-helm.yaml
```
