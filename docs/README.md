# Documentation for some resources

## install k3s

### Install on Master
```bash
curl -sfL https://get.k3s.io | sh -s - --config $(pwd)/config.yaml
```

### Show token
```bash
cat /var/lib/rancher/k3s/server/node-token
```

### Install on Worker

#### Set token env variable
```bash
K3S_TOKEN=
```

#### Export env variable
```bash
export K3S_TOKEN
```

#### Install k3s as worker
```bash
curl -sfL https://get.k3s.io | K3S_URL=https://MASTER:6443 sh -

curl -sfL https://get.k3s.io | K3S_URL=https://panda-nomada.thebeijinho.com:6443 sh -
```

## Check install
```bash
k3s check-config
```

## Show kubeconfig
```bash
cat /etc/rancher/k3s/k3s.yaml
```

---

## Argo CD

### 1 - Install Argo CD
```bash
kubectl create namespace argocd

kubectl apply -n argocd -k ./resources/argocd
```

### 2 - Install the app of apps
```bash
kubectl apply --filename ./resources/gitops/resources/project-gitops.yaml

kubectl apply --filename ./resources/gitops/gitops-app.yaml
```
