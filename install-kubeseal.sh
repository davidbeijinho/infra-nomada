#!/bin/sh

wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.20.2/kubeseal-0.20.2-linux-amd64.tar.gz

tar -xvzf kubeseal-0.20.2-linux-amd64.tar.gz kubeseal

sudo install -m 755 kubeseal /usr/local/bin/kubeseal

rm kubeseal-0.20.2-linux-amd64.tar.gz

rm ./kubeseal
