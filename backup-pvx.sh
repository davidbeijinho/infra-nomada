#!/bin/sh

kubectl get --all-namespaces -o yaml pvc > ./backup-pvc.yaml 
kubectl get --all-namespaces -o yaml pv > ./backup-pv.yaml 
