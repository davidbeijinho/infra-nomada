#!/bin/sh

if [ -z "$1" ]; then
	echo "need a name"
    updateAll
elif [ "$1" = "urso" ]; then
	kubectl config use-context urso-nomada
	kubeseal <mysecret.yaml >mynamespacedsealedsecret.yaml --controller-name=sealed-secrets -o yaml -n "$1"
elif [ "$1" = "panda" ]; then
	kubectl config use-context panda-nomada
	kubeseal <mysecret.yaml >mynamespacedsealedsecret.yaml --controller-name=sealed-secrets -o yaml -n "$1"
elif [ "$1" = "mood" ]; then
	kubectl config use-context mood-nomada
	kubeseal <mysecret.yaml >mynamespacedsealedsecret.yaml --controller-name=sealed-secrets -o yaml -n "$1"
elif [ "$1" = "mini" ]; then
	kubectl config use-context mini-nomada
	kubeseal <mysecret.yaml >mynamespacedsealedsecret.yaml --controller-name=sealed-secrets -o yaml -n "$1"
elif [ "$1" = "maxi" ]; then
	kubectl config use-context maxi-nomada
	kubeseal <mysecret.yaml >mynamespacedsealedsecret.yaml --controller-name=sealed-secrets -o yaml -n "$1"
elif [ "$1" = "contabo" ]; then
	kubectl config use-context contabo-nomada
	kubeseal <mysecret.yaml >mynamespacedsealedsecret.yaml --controller-name=sealed-secrets -o yaml -n "$1"
else
	echo "no matching do nothing"
fi
