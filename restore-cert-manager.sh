#!/bin/sh

# kubectl get --all-namespaces -oyaml issuer,clusterissuer,cert > backup-cert-all.yaml && \
# kubectl get --all-namespaces secrets  -oyaml -l 'controller.cert-manager.io/fao' > backup-secrets-cert.yaml && \
# kubectl get --all-namespaces namespaces -oyaml   > backup-namespaces.yaml
# kubectl get pvc,pv -A  --field-selector metadata.namespace=tiddlywiki -oyaml > backup-pvc-tidd.yaml && \
# kubectl get pvc,pv -A  --field-selector metadata.namespace=thunderbird -oyaml > backup-pvc-thund.yaml && \
# kubectl get pvc,pv -A  --field-selector metadata.namespace=grocy -oyaml >backup-pvc-grocy.yaml
# kubectl get pv --namespace=tiddlywiki -oyaml > backup-pc-tidd.yaml && \
# kubectl get pv --namespace=thunderbird -oyaml > backup-pc-thund.yaml && \
# kubectl get pv --namespace=grocy -oyaml >backup-pc-grocy.yaml

# kubectl apply -f ./mini-nomada/resources/cert-manager/resources-helm.yaml && \
# kubectl apply -f ./mini-nomada/resources/longhorn/accounts.yaml  && \
kubectl apply -f <(awk '!/^ *(resourceVersion|uid): [^ ]+$/' backup-namespaces.yaml)  && \
kubectl apply -f <(awk '!/^ *(resourceVersion|uid): [^ ]+$/' backup-secrets-cert.yaml)  && \
kubectl apply -f <(awk '!/^ *(resourceVersion|uid): [^ ]+$/' backup-cert-all.yaml) && \
kubectl apply -f /data/projects/infra-nomada/mini-nomada/resources/longhorn/accounts.yaml
