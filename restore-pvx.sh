#!/bin/sh

kubectl apply  -f <(awk '!/^ *(resourceVersion|uid): [^ ]+$/' backup-pv.yaml)
kubectl apply  -f <(awk '!/^ *(resourceVersion|uid): [^ ]+$/' backup-pvc.yaml)

