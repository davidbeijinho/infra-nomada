#!/bin/sh

real_path=$(realpath "$0")
dir_path=$(dirname "$real_path")

if [ -z "$1" ]; then
	echo "need a context"
elif [ "$1" = "maxi" ]; then
	kubeseal --recovery-unseal -o yaml --recovery-private-key "$dir_path"/master-maxi.yaml <"$2" >myunsealedsecret.yaml 
# elif [ "$1" = "mini" ]; then
# 	kubeseal --recovery-unseal -o yaml --recovery-private-key "$dir_path"/master-mini.yaml <"$2" >myunsealedsecret.yaml
elif [ "$1" = "panda" ]; then
	kubeseal --recovery-unseal -o yaml --recovery-private-key "$dir_path"/master-panda.yaml <"$2" >myunsealedsecret.yaml
elif [ "$1" = "contabo" ]; then
	kubeseal --recovery-unseal -o yaml --recovery-private-key "$dir_path"/master-contabo.yaml <"$2" >myunsealedsecret.yaml 
else
	echo "no matching do nothing"
fi
