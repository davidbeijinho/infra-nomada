#!/bin/sh

real_path=$(realpath "$0")
dir_path=$(dirname "$real_path")
kube_version=v1.27.6+k3s1
# check https://artifacthub.io/

kubectl config use-context contabo-nomada

### adminer
### argo-cd
helm template argocd argo/argo-cd --debug --dry-run --include-crds --namespace argocd --kube-version=$kube_version --values="$dir_path/resources/argo-cd/values.yaml" >"$dir_path/resources/argo-cd/resources-helm.yaml"
### cert-manager
helm template cert-manager jetstack/cert-manager --debug --dry-run --include-crds --namespace cert-manager --kube-version=$kube_version --values="$dir_path/resources/cert-manager/values.yaml" >"$dir_path/resources/cert-manager/resources-helm.yaml"
### cloudflare-dns
### directus-agency
# helm template directus-agency directus/directus --debug --dry-run --include-crds --namespace directus-agency --kube-version=$kube_version --values="$dir_path/resources/directus-agency/values.yaml" >"$dir_path/resources/directus-agency/resources-helm.yaml"
### directus-dev
# helm template directus-dev directus/directus --debug --dry-run --include-crds --namespace directus-dev --kube-version=$kube_version --values="$dir_path/resources/directus-dev/values.yaml" >"$dir_path/resources/directus-dev/resources-helm.yaml"
### directus-prod
# helm template directus-prod directus/directus --debug --dry-run --include-crds --namespace directus-prod --kube-version=$kube_version --values="$dir_path/resources/directus-prod/values.yaml" >"$dir_path/resources/directus-prod/resources-helm.yaml"
### filebrowser
### gitops
### local-path-storage
### mariadb
helm template mariadb bitnami/mariadb --debug --dry-run --include-crds --namespace mariadb --kube-version=$kube_version --values="$dir_path/resources/mariadb/values.yaml" >"$dir_path/resources/mariadb/resources-helm.yaml"
### minio
### n8n
# helm template n8n open-8gears/n8n --debug --dry-run --include-crds --namespace n8n --kube-version=$kube_version --values="$dir_path/resources/n8n/values.yaml" >"$dir_path/resources/n8n/resources-helm.yaml"
# helm template n8n one-acre-fund/n8n --debug --dry-run --include-crds --namespace n8n --kube-version=$kube_version --values="$dir_path/resources/n8n/values.yaml" >"$dir_path/resources/n8n/resources-helm.yaml"
### nginx
helm template nginx bitnami/nginx --debug --dry-run --include-crds --namespace nginx --kube-version=$kube_version --values="$dir_path/resources/nginx/values.yaml" >"$dir_path/resources/nginx/resources-helm.yaml"
### postgresql
### redis
helm template redis bitnami/redis --debug --dry-run --include-crds --namespace redis --kube-version=$kube_version --values="$dir_path/resources/redis/values.yaml" >"$dir_path/resources/redis/resources-helm.yaml"
### sealed-secrets
helm template sealed-secrets sealed-secrets/sealed-secrets --debug --dry-run --include-crds --namespace kube-system --kube-version=$kube_version --values="$dir_path/resources/sealed-secrets/values.yaml" >"$dir_path/resources/sealed-secrets/resources-helm.yaml"
### traefik
helm template traefik traefik/traefik --debug --dry-run --include-crds --namespace default --kube-version=$kube_version --values="$dir_path/resources/traefik/values.yaml" >"$dir_path/resources/traefik/resources-helm.yaml"
### whoami

# ARCHIVE

# helm template couchdb couchdb/couchdb --debug --dry-run --include-crds --namespace couchdb --kube-version=$kube_version --values="$dir_path/resources/couchdb/values.yaml" >"$dir_path/resources/couchdb/resources-helm.yaml"
# helm template supabase bitnami/supabase --debug --dry-run --include-crds --namespace supabase --kube-version=$kube_version --values="$dir_path/resources/supabase/values.yaml" >"$dir_path/resources/supabase/resources-helm.yaml"
# helm template supabase /data/projects/SANDBOX/supabase-kubernetes/charts/supabase --debug --dry-run --include-crds --namespace supabase --kube-version=$kube_version --values="$dir_path/resources/supabase/values.yaml" >"$dir_path/resources/supabase/resources-helm.yaml"
# helm template minio minio/minio --debug --dry-run --include-crds --namespace minio --kube-version=$kube_version --values="$dir_path/resources/minio/values.yaml" >"$dir_path/resources/minio/resources-helm.yaml"
# helm template redis bitnami/redis --debug --dry-run --include-crds --namespace redis --kube-version=$kube_version --values="$dir_path/resources/redis/values.yaml" >"$dir_path/resources/redis/resources-helm.yaml"
# helm template budibase budibase/budibase --debug --dry-run --include-crds --namespace budibase --kube-version=$kube_version --values="$dir_path/resources/budibase/values.yaml" >"$dir_path/resources/budibase/resources-helm.yaml"