#!/bin/sh

if [ -z "$1" ]; then
	echo "need a context"
elif [ "$1" = "maxi" ]; then
    kubectl config use-context maxi-nomada
    kubectl get secret -n kube-system -l sealedsecrets.bitnami.com/sealed-secrets-key -o yaml >master-maxi.yaml
# elif [ "$1" = "mini" ]; then
    # kubectl config use-context mini-nomada
    # kubectl get secret -n kube-system -l sealedsecrets.bitnami.com/sealed-secrets-key -o yaml >master-mini.yaml
elif [ "$1" = "panda" ]; then
    kubectl config use-context panda-nomada
    kubectl get secret -n kube-system -l sealedsecrets.bitnami.com/sealed-secrets-key -o yaml >master-panda.yaml
elif [ "$1" = "contabo" ]; then
    kubectl config use-context contabo-nomada
    kubectl get secret -n kube-system -l sealedsecrets.bitnami.com/sealed-secrets-key -o yaml >master-contabo.yaml
else
	echo "no matching do nothing"
fi
