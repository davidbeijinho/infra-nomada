#!/bin/sh

date_str="2023-06-27_20:15:06"
file_ext=".yaml"
other_file="./backups/backup_other_$date_str$file_ext"
secrets_file="./backups/backup_secrets_$date_str$file_ext"

# kubectl apply -f <(awk '!/^ *(resourceVersion|uid): [^ ]+$/' $secrets_file)

kubectl apply dry-run -f <(awk '!/^ *(resourceVersion|uid): [^ ]+$/' $other_file)
