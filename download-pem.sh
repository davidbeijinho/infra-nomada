#!/bin/sh


#!/bin/sh

if [ -z "$1" ]; then
	echo "need a context"
elif [ "$1" = "maxi" ]; then
    kubectl config use-context maxi-nomada
    kubeseal --controller-namespace=kube-system --controller-name=sealed-secrets --fetch-cert >key-maxi.pem
# elif [ "$1" = "mini" ]; then
    # kubectl config use-context mini-nomada
    # kubeseal --controller-namespace=kube-system --controller-name=sealed-secrets --fetch-cert >key-mini.pem
elif [ "$1" = "panda" ]; then
    kubectl config use-context panda-nomada
    kubeseal --controller-namespace=kube-system --controller-name=sealed-secrets --fetch-cert >key-panda.pem
elif [ "$1" = "contabo" ]; then
    kubectl config use-context contabo-nomada
    kubeseal --controller-namespace=kube-system --controller-name=sealed-secrets --fetch-cert >key-contabo.pem
else
	echo "no matching do nothing"
fi
