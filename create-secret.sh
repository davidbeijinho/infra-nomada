#!/bin/sh

echo SECRET_VALUE | kubectl create secret generic SECRET_NAME --dry-run=client --namespace NAMESPACE --from-file=KEY_NAME=/dev/stdin -o yaml >mysecret.yaml
