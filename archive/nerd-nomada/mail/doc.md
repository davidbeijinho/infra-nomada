# MAIL

## download setup script
```bash
wget https://raw.githubusercontent.com/docker-mailserver/docker-mailserver/master/setup.sh
```

## make it executable
chmod a+x ./setup.sh

## enable dkim

./setup.sh config dkim

## get the value
 cat ./docker-data/dms/config/opendkim/keys/thebeijinho.com/mail.txt

## test it

dig mail._domainkey.thebeijinho.com TXT

## add DMARC

_dmarc.thebeijinho.com. IN TXT "v=DMARC1; p=none; rua=mailto:dmarc.report@thebeijinho.com; ruf=mailto:dmarc.report@thebeijinho.com; sp=none; ri=86400"

## add SPF
; MX record must be declared for SPF to work
thebeijinho.com. IN  MX 1 mail.thebeijinho.com.

; SPF record
thebeijinho.com. IN TXT "v=spf1 mx ~all"

## create first account

./setup.sh email add me@thebeijinho.com

### to be reviewed

<https://docker-mailserver.github.io/docker-mailserver/edge/faq/#how-can-i-configure-a-catch-all>


## traefik

touch /root/MAIL/docker-data/traefik/acme.json && \
chmod 600 /root/MAIL/docker-data/traefik/acme.json

@thebeijinho.com me@thebeijinho.com
