---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: tiddlywiki-app
spec:
  replicas: 1
  revisionHistoryLimit: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: tiddlywiki-app
  template:
    metadata:
      labels:
        app: tiddlywiki-app
    spec:
      terminationGracePeriodSeconds: 1000
      nodeSelector:
        kubernetes.io/arch: amd64
      containers:
        - name: tiddlywiki-app
          env:
            - name: PUID
              value: "1000"
            - name: PGID
              value: "1000"
          command:
            - "/sbin/tini"
            - "--"
            - "tiddlywiki"
            - "neowiki"
            - "--listen"
            - "host=0.0.0.0"
          image: registry.gitlab.com/davidbeijinho/tiddlywiki-docker:5.3.0
          volumeMounts:
            - name: data
              mountPath: /tiddlywiki
        - name: git-push
          env:
            - name: PUID
              value: "1000"
            - name: PGID
              value: "1000"
          command: ["/bin/sh", "-c"]
          args:
            - git config --global --add safe.directory /repo &&
              git config --global user.email "davidbeijinho@gmail.com" &&
              git config --global user.name "David Beijinho" &&
              /scripts/git-auto -r -p -b main -i 30 -s origin -d /repo
              # sleep 2000
              # git clone https://gitlab.com/davidbeijinho/neo-wiki-nomada /repo
          image: bitnami/git:2.42.0
          volumeMounts:
            - name: data
              mountPath: /repo
            - name: git-auto-script
              mountPath: /scripts/git-auto
              readOnly: true
              subPath: git-auto
            - name: ssh-nomada
              mountPath: /root/.ssh/id_rsa
              subPath: id_rsa
            - name: ssh-nomada-pub
              mountPath: /root/.ssh/id_rsa.pub
              subPath: id_rsa.pub
            - name: known-hosts
              mountPath: /root/.ssh/known_hosts
              subPath: known_hosts
      volumes:
        - name: data
          persistentVolumeClaim:
            claimName: neo-wiki-nfs-vol
        - name: git-auto-script
          configMap:
            name: git-auto-script
            defaultMode: 0744
            items:
              - key: git-auto
                path: git-auto
        - name: ssh-nomada-pub
          secret:
            items:
            - key: id_rsa.pub
              path: id_rsa.pub
            secretName: ssh-keys-mini-nomada
            defaultMode: 0600
        - name: ssh-nomada
          secret:
            items:
            - key: id_rsa
              path: id_rsa
            secretName: ssh-keys-mini-nomada
            defaultMode: 0600
        - name: known-hosts
          configMap:
            name: known-hosts
            items:
            - key: "known_hosts"
              path: "known_hosts"
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: git-auto-script
data:
  # last commit https://github.com/logseq/git-auto/commit/04a9d5d19fbcaef061067696d6761faaea89a420
  git-auto: |
    #!/usr/bin/env bash

    ## Tested on macOS big sur 11.2.1
    ## Usage:
    ##  git-auto ;; use current script dir as git dir, and auto commit, not push.
    ##  git-auto -d /path/to/your/note's/dir   ;; set git dir
    ##  git-auto -p ;; auto commit and push
    ## git-auto -r ;; auto commit, rebase, merge, push
    ##  git-auto -s origin -p ;; set remote server
    ##  git-auto -b main -p ;; set git branch
    ##  git-auto -i 30 -p ;; set interval seconds
    ##  git-auto -o -p;; execute once


    #set -e
    #set -x

    usage="usage: $0
        [-d <git directory>]
        [-i <interval seconds>]
        [-p <push to remote server>]
        [-r rebase before pushing changes]
        [-s git remote server]
        [-b git branch]
        [-o <execute once]"

    push_to_server=0
    server=origin
    interval=20
    once=0

    OPTIND=1
    while getopts d:i:b:s:pro flag; do
      case "${flag}" in
      d) directory=${OPTARG} ;;
      p) push_to_server=1 ;;
      r) rebase=1 ;;
      o) once=1 ;;
      s) server=${OPTARG} ;;
      b) branch=${OPTARG} ;;
      i) interval=${OPTARG} ;;
      *)
        echo "ERROR: ${usage}" >&2
        exit 1
        ;;
      esac
    done
    shift $((OPTIND - 1))

    if  [[ "${directory}" ]]; then
      cd "${directory}" || exit 1
    fi

    if [[ -z "${branch}" ]]; then
      branch=$(git rev-parse --abbrev-ref HEAD)
    fi

    get-commit-message() {
        local commit_message=$(git diff --name-only HEAD~1..HEAD)
        commit_message=$(echo "${commit_message}" | sed -e 's/^.*\///')
        echo "${commit_message}"
    }

    auto-commit-and-push() {
      if ! [[ $(git status) =~ "working tree clean" ]]; then
        git add .
        git commit -m "$(get-commit-message)"

        if [[ 1 == ${rebase} ]]; then
          git pull --rebase
        fi

        if [[ 1 == "${push_to_server}" ]]; then
          git push "${server}" "${branch}"
        fi
      fi
    }

    date

    if [[ 1 == "${once}" ]]; then
      auto-commit-and-push
    else
      while true; do
        auto-commit-and-push
        sleep "${interval}"
      done
    fi
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: neo-wiki-nfs-vol
  annotations:
    nfs.io/storage-path: neo-wiki
spec:
  accessModes:
    - ReadWriteMany
  storageClassName: nfs-configs
  resources:
    requests:
      storage: 5Gi
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: known-hosts
data:
  known_hosts: |
    gitlab.com ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf
    |1|JPEZ7R0lC7wTvXi9hXnKWEwH4sw=|eAenuFcWaP7Aqox4EANXM128oF0= ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf
