#!/bin/sh

real_path=$(realpath "$0")
dir_path=$(dirname "$real_path")
kube_version=v1.27.6+k3s1
# check https://artifacthub.io/

kubectl config use-context mini-nomada

# adminer
helm template argocd argo/argo-cd --debug --dry-run --include-crds --namespace argocd --kube-version=$kube_version --values="$dir_path/resources/argo-cd/values.yaml" >"$dir_path/resources/argo-cd/resources-helm.yaml"
helm template argo-events argo/argo-events --debug --dry-run --include-crds --namespace kube-system --kube-version=$kube_version --values="$dir_path/resources/argo-events/values.yaml" >"$dir_path/resources/argo-events/resources-helm.yaml"
helm template argo-workflows argo/argo-workflows --debug --dry-run --include-crds --namespace kube-system --kube-version=$kube_version --values="$dir_path/resources/argo-workflows/values.yaml" >"$dir_path/resources/argo-workflows/resources-helm.yaml"
# helm template audiobookshelf k8s-at-home/audiobookshelf --debug --dry-run --namespace audiobookshelf --kube-version=$kube_version --values="$dir_path/resources/audiobookshelf/values.yaml" >"$dir_path/resources/audiobookshelf/resources-helm.yaml"
helm template authelia authelia/authelia --debug --dry-run --include-crds --namespace auth --kube-version=$kube_version --values="$dir_path/resources/authelia/values.yaml" >"$dir_path/resources/authelia/resources-helm.yaml"
helm template awx-operator awx-operator/awx-operator --debug --dry-run --include-crds --namespace awx-operator --kube-version=$kube_version --values="$dir_path/resources/awx-operator/values.yaml" >"$dir_path/resources/awx-operator/resources-helm.yaml"
# helm template bazarr k8s-at-home/bazarr --debug --dry-run --include-crds --namespace bazarr --kube-version=$kube_version --values="$dir_path/resources/bazarr/values.yaml" >"$dir_path/resources/bazarr/resources-helm.yaml"
# calibre
helm template cert-manager jetstack/cert-manager --debug --dry-run --include-crds --namespace cert-manager --kube-version=$kube_version --values="$dir_path/resources/cert-manager/values.yaml" >"$dir_path/resources/cert-manager/resources-helm.yaml"
# change detection ?????
# cloudflare-dns no helm chart
helm template filebrowser k8s-at-home/filebrowser --debug --dry-run --include-crds --namespace filebrowser --kube-version=$kube_version --values="$dir_path/resources/filebrowser/values.yaml" >"$dir_path/resources/filebrowser/resources-helm.yaml"
# gitops app of apps
helm template grafana grafana/grafana --debug --dry-run --include-crds --namespace grafana --kube-version=$kube_version --values="$dir_path/resources/grafana/values.yaml" >"$dir_path/resources/grafana/resources-helm.yaml"
helm template grocy k8s-at-home/grocy --debug --dry-run --include-crds --namespace grocy --kube-version=$kube_version --values="$dir_path/resources/grocy/values.yaml" >"$dir_path/resources/grocy/resources-helm.yaml"
helm template home-assistant k8s-at-home/home-assistant --debug --dry-run --include-crds --namespace home-assistant --kube-version=$kube_version --values="$dir_path/resources/home-assistant/values.yaml" >"$dir_path/resources/home-assistant/resources-helm.yaml"
helm template homer k8s-at-home/homer --debug --dry-run --include-crds --namespace homer --kube-version=$kube_version --values="$dir_path/resources/homer/values.yaml" >"$dir_path/resources/homer/resources-helm.yaml"
# jackett
# jellyfin
# jellyserr ?????
# lidarr
# local path storage
helm template loki-stack grafana/loki-stack --debug --dry-run --include-crds --namespace loki-stack --kube-version=$kube_version --values="$dir_path/resources/loki-stack/values.yaml" >"$dir_path/resources/loki-stack/resources-helm.yaml"
helm template prometheus-smartctl-exporter prometheus-community/prometheus-smartctl-exporter --debug --dry-run --include-crds --namespace loki-stack --kube-version=$kube_version --values="$dir_path/resources/loki-stack/prometheus-smartctl-exporter-values.yaml" >"$dir_path/resources/loki-stack/resources-prometheus-smartctl-exporter-helm.yaml"

helm template longhorn  longhorn/longhorn --debug --dry-run --include-crds --namespace longhorn-system --kube-version=$kube_version --values="$dir_path/resources/longhorn/values.yaml" >"$dir_path/resources/longhorn/resources-helm.yaml"
helm template mariadb bitnami/mariadb --debug --dry-run --include-crds --namespace mariadb --kube-version=$kube_version --values="$dir_path/resources/mariadb/values.yaml" >"$dir_path/resources/mariadb/resources-helm.yaml"
helm template minio minio/minio --debug --dry-run --include-crds --namespace minio --kube-version=$kube_version --values="$dir_path/resources/minio/values.yaml" >"$dir_path/resources/minio/resources-helm.yaml"
# minio-remote
helm template mosquitto k8s-at-home/mosquitto --debug --dry-run --include-crds --namespace mosquitto --kube-version=$kube_version --values="$dir_path/resources/mosquitto/values.yaml" >"$dir_path/resources/mosquitto/resources-helm.yaml"
helm template nextcloud nextcloud/nextcloud --debug --dry-run --include-crds --namespace nextcloud --kube-version=$kube_version --values="$dir_path/resources/nextcloud/values.yaml" >"$dir_path/resources/nextcloud/resources-helm.yaml"
helm template nfs-configs nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --debug --dry-run --include-crds --namespace nfs-configs --kube-version=$kube_version --values="$dir_path/resources/nfs-configs/values.yaml" >"$dir_path/resources/nfs-configs/resources-helm.yaml"
helm template nginx bitnami/nginx --debug --dry-run --include-crds --namespace nginx --kube-version=$kube_version --values="$dir_path/resources/nginx/values.yaml" >"$dir_path/resources/nginx/resources-helm.yaml"
# nodered https://artifacthub.io/packages/helm/truecharts/node-red
# octoprint
# panda-nomada
helm template pgadmin4 runix/pgadmin4 --debug --dry-run --include-crds --namespace pgadmin4 --kube-version=$kube_version --values="$dir_path/resources/pgadmin4/values.yaml" >"$dir_path/resources/pgadmin4/resources-helm.yaml"
helm template photoprism p80n/photoprism --debug --dry-run --include-crds --namespace photoprism --kube-version=$kube_version --values="$dir_path/resources/photoprism/values.yaml" >"$dir_path/resources/photoprism/resources-helm.yaml"
# pocketbase
# helm template postgresql bitnami/postgresql --debug --dry-run --include-crds --namespace postgresql --kube-version=$kube_version --values="$dir_path/resources/postgresql/values.yaml" >"$dir_path/resources/postgresql/resources-helm.yaml"
# prowlarr
# radarr
# rathole?
# readarr
# helm template redis bitnami/redis --debug --dry-run --include-crds --namespace redis --kube-version=$kube_version --values="$dir_path/resources/redis/values.yaml" >"$dir_path/resources/redis/resources-helm.yaml"
# helm template renovate renovate/renovate                   --debug --dry-run --include-crds --namespace renovate        --kube-version=$kube_version --values="$dir_path/resources/renovate/values.yaml"        >"$dir_path/resources/renovate/resources-helm.yaml"
# restserver
helm template sealed-secrets sealed-secrets/sealed-secrets --debug --dry-run --include-crds --namespace kube-system --kube-version=$kube_version --values="$dir_path/resources/sealed-secrets/values.yaml" >"$dir_path/resources/sealed-secrets/resources-helm.yaml"
helm template shlink-web christianknell/shlink-web --debug --dry-run --include-crds --namespace shlink-web --kube-version=$kube_version --values="$dir_path/resources/shlink-web/values.yaml" >"$dir_path/resources/shlink-web/resources-helm.yaml"
helm template shlink-backend christianknell/shlink-backend --debug --dry-run --include-crds --namespace shlink-backend --kube-version=$kube_version --values="$dir_path/resources/shlink-backend/values.yaml" >"$dir_path/resources/shlink-backend/resources-helm.yaml"
# sonarr
# syncthing https://artifacthub.io/packages/helm/truecharts/syncthing
# thunderbird
# helm template tooljet tooljet/tooljet --debug --dry-run --include-crds --namespace tooljet --kube-version=$kube_version --values="$dir_path/resources/tooljet/values.yaml" >"$dir_path/resources/tooljet/resources-helm.yaml"
helm template traefik traefik/traefik --debug --dry-run --include-crds --namespace default --kube-version=$kube_version --values="$dir_path/resources/traefik/values.yaml" >"$dir_path/resources/traefik/resources-helm.yaml"
# transmission
helm template trilium-notes  ohdearaugustin/trilium-notes --debug --dry-run --include-crds --namespace trilium-notes-system --kube-version=$kube_version --values="$dir_path/resources/trilium-notes/values.yaml" >"$dir_path/resources/trilium-notes/resources-helm.yaml"
helm template velero vmware-tanzu/velero --debug --dry-run --include-crds --namespace velero --kube-version=$kube_version --values="$dir_path/resources/velero/values.yaml" >"$dir_path/resources/velero/resources-helm.yaml"
helm template wallabag geek-cookbook/wallabag --dry-run --include-crds --namespace wallabag --debug --kube-version=$kube_version --values="$dir_path/resources/wallabag/values.yaml" >"$dir_path/resources/wallabag/resources-helm.yaml"
# whoami https://artifacthub.io/packages/helm/philippwaller/whoami
helm template wordpress bitnami/wordpress --debug --dry-run --include-crds --namespace wordpress --kube-version=$kube_version --values="$dir_path/resources/wordpress/values.yaml" >"$dir_path/resources/wordpress/resources-helm.yaml"

# ARCHIVE
# helm template supabase /data/projects/SANDBOX/supabase-kubernetes/charts/supabase --debug --dry-run --include-crds --namespace supabase --kube-version=$kube_version --values="$dir_path/resources/supabase/values.yaml" >"$dir_path/resources/supabase/resources-helm.yaml"
# helm template supabase bitnami/supabase --debug --dry-run --include-crds --namespace supabase --kube-version=$kube_version --values="$dir_path/resources/supabase/values.yaml" >"$dir_path/resources/supabase/resources-helm.yaml"
# helm template supabase /data/projects/SANDBOX/supabase-kubernetes/charts/supabase --debug --dry-run --include-crds --namespace supabase --kube-version=$kube_version --values="$dir_path/resources/supabase/values.yaml" >"$dir_path/resources/supabase/resources-helm.yaml"
# helm template appsmith appsmith/appsmith --debug --dry-run --include-crds --namespace appsmith --kube-version=$kube_version --values="$dir_path/resources/appsmith/values.yaml" >"$dir_path/resources/appsmith/resources-helm.yaml"
# helm template lowcoder /data/projects/SANDBOX/lowcoder/lowcoder/deploy/helm --debug --dry-run --include-crds --namespace lowcoder --kube-version=$kube_version --values="$dir_path/resources/lowcoder/values.yaml" >"$dir_path/resources/lowcoder/resources-helm.yaml"
# helm template windmill windmill/windmill --debug --dry-run --include-crds --namespace windmill --kube-version=$kube_version --values="$dir_path/resources/windmill/values.yaml" >"$dir_path/resources/windmill/resources-helm.yaml"
# helm template budibase budibase/budibase --debug --dry-run --include-crds --namespace budibase --kube-version=$kube_version --values="$dir_path/resources/budibase/values.yaml" >"$dir_path/resources/budibase/resources-helm.yaml"
# helm template nfs-media nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --debug --dry-run --include-crds --namespace nfs-media --kube-version=$kube_version --values="$dir_path/resources/nfs-media/values.yaml" >"$dir_path/resources/nfs-media/resources-helm.yaml"
# helm template nfs-docs nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --debug --dry-run --include-crds --namespace nfs-docs --kube-version=$kube_version --values="$dir_path/resources/nfs-docs/values.yaml" >"$dir_path/resources/nfs-docs/resources-helm.yaml"
# helm template nfs-server-provisioner nfs-ganesha-server-and-external-provisioner/nfs-server-provisioner --debug --dry-run --include-crds --namespace nfs-server-provisioner --kube-version=$kube_version --values="$dir_path/resources/nfs-server-provisioner/values.yaml" >"$dir_path/resources/nfs-server-provisioner/resources-helm.yaml"
# helm template nfs-server obeone/nfs-server --debug --dry-run --include-crds --namespace nfs-server --kube-version=$kube_version --values="$dir_path/resources/nfs-server/values.yaml" >"$dir_path/resources/nfs-server/resources-helm.yaml"
# helm template rook-ceph rook-release/rook-ceph --debug --dry-run --include-crds --namespace rook-ceph --kube-version=$kube_version --values="$dir_path/resources/rook-ceph/values.yaml" >"$dir_path/resources/rook-ceph/resources-helm.yaml"
# helm template rancher rancher-latest/rancher --debug --dry-run --include-crds --namespace cattle-system --kube-version=$kube_version --values="$dir_path/resources/rancher/values.yaml" >"$dir_path/resources/rancher/resources-helm.yaml"
# helm template wger github-wger/wger --debug --dry-run --include-crds --namespace wger --kube-version=$kube_version --values="$dir_path/resources/wger/values.yaml" >"$dir_path/resources/wger/resources-helm.yaml"
# helm template yourls yourls/yourls --debug --dry-run --include-crds --namespace yourls --kube-version=$kube_version --values="$dir_path/resources/yourls/values.yaml" >"$dir_path/resources/yourls/resources-helm.yaml"
# helm template mealie smarthall/mealie --debug --dry-run --include-crds --namespace mealie --kube-version=$kube_version --values="$dir_path/resources/mealie/values.yaml" >"$dir_path/resources/mealie/resources-helm.yaml"
# helm template tandoor gabe565/tandoor --debug --dry-run --include-crds --namespace tandoor --kube-version=$kube_version --values="$dir_path/resources/tandoor/values.yaml" >"$dir_path/resources/tandoor/resources-helm.yaml"
# frpc no helm chart
# helm template synapse halkeye/synapse                      --debug --dry-run --include-crds --namespace synapse         --kube-version=$kube_version --values="$dir_path/resources/synapse/values.yaml"         >"$dir_path/resources/synapse/resources-helm.yaml"
# helm template radicale k8s-at-home/radicale --dry-run --include-crds --namespace radicale --debug --kube-version=$kube_version --values="$dir_path/resources/radicale/values.yaml" >"$dir_path/resources/radicale/resources-helm.yaml"
# helm template mailpile TrueCharts/mailpile --dry-run --include-crds --namespace mailpile --debug --kube-version=$kube_version --values="$dir_path/resources/mailpile/values.yaml" >"$dir_path/resources/mailpile/resources-helm.yaml"
# helm template cloudflared pascaliske/cloudflared                  --debug --dry-run --include-crds --namespace cloudflared       --kube-version=$kube_version --values="$dir_path/resources/cloudflared/values.yaml"       >"$dir_path/resources/cloudflared/resources-helm.yaml"d
# helm template drone drone/drone --dry-run --include-crds --namespace auth --debug --kube-version=$kube_version --values="$dir_path/resources/drone/values.yaml" >"$dir_path/resources/drone/resources-helm.yaml"
# helm template gitea gitea-charts/gitea --dry-run --include-crds --namespace gitea --debug --kube-version=$kube_version --values="$dir_path/resources/gitea/values.yaml" >"$dir_path/resources/gitea/resources-helm.yaml"
# helm template openproject truecharts/openproject --dry-run --include-crds --namespace openproject --debug --kube-version=$kube_version --values="$dir_path/resources/openproject-truecharts/values.yaml" >"$dir_path/resources/openproject-truecharts/resources-helm.yaml"
# helm template uptime-kuma k8s-at-home/uptime-kuma --dry-run --include-crds --namespace uptime-kuma --debug --kube-version=$kube_version --values="$dir_path/resources/uptime-kuma/values.yaml" >"$dir_path/resources/uptime-kuma/resources-helm.yaml"
# helm template wallabag k8s-at-home/wallabag --dry-run --include-crds --namespace wallabag --debug --kube-version=$kube_version --values="$dir_path/resources/wallabag/values.yaml" >"$dir_path/resources/wallabag/resources-helm.yaml"
# helm template wallabag halkeye/wallabag  --dry-run --include-crds --namespace wallabag --debug --kube-version=$kube_version --values="$dir_path/resources/wallabag/values-1.yaml" >"$dir_path/resources/wallabag/resources-helm-1.yaml"
# helm template kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard --dry-run --include-crds --namespace kubernetes-dashboard --debug --kube-version=$kube_version --values="$dir_path/resources/kubernetes-dashboard/values.yaml" >"$dir_path/resources/kubernetes-dashboard/resources-helm.yaml"
# helm template teleport teleport/teleport-kube-agent                  --debug --dry-run --include-crds --namespace teleport       --kube-version=$kube_version --values="$dir_path/resources/teleport/values.yaml"       >"$dir_path/resources/teleport/resources-helm.yaml"

# does not support v1.25 yet
# helm template borgserver lib42/borgserver                  --debug --dry-run --include-crds --namespace borgserver       --kube-version=$kube_version --values="$dir_path/resources/borgserver/values.yaml"       >"$dir_path/resources/borgserver/resources-helm.yaml"

# does not work
# helm template duckdns        ebrianne.github.io/cert-manager-webhook-duckdns --dry-run --include-crds --namespace cert-manager --debug --kube-version=$kube_version --values="$dir_path/resources/duckdns/values.yaml"         > "$dir_path/resources/duckdns/resources-helm.yaml"
