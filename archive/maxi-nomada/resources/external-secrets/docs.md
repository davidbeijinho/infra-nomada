create secret

kubectl create secret generic bitwarden-access-token -n <YOUR_NAMESPACE> --from-literal=token="<TOKEN_HERE>"

kubectl create secret generic bitwarden-access-token -n external-secrets --from-literal=token="<TOKEN_HERE>"
