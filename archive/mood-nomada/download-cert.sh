#!/bin/sh

kubectl config use-context mood-nomada

kubectl get secret -n kube-system -l sealedsecrets.bitnami.com/sealed-secrets-key -o yaml >master-mood.yaml
