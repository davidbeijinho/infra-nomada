#!/bin/sh

if [ -z "$1" ]; then
	echo "please specify a secret"
else
	real_path=$(realpath "$0")
	dir_path=$(dirname "$real_path")
	kubeseal --recovery-unseal -o yaml --recovery-private-key "$dir_path"/../master-mood.yaml <"$1" >myunsealedsecret.yaml
fi
