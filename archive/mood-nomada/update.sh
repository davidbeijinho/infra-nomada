#!/bin/sh

real_path=$(realpath "$0")
dir_path=$(dirname "$real_path")

helm template argocd argo/argo-cd --dry-run --include-crds --namespace argocd --debug --values="$dir_path/resources/argo-cd/values.yaml" >"$dir_path/resources/argo-cd/resources-helm.yaml"
helm template traefik traefik/traefik --dry-run --include-crds --namespace default --debug --values="$dir_path/resources/traefik/values.yaml" >"$dir_path/resources/traefik/resources-helm.yaml"
helm template sealed-secrets sealed-secrets/sealed-secrets --dry-run --include-crds --namespace kube-system --debug --values="$dir_path/resources/sealed-secrets/values.yaml" >"$dir_path/resources/sealed-secrets/resources-helm.yaml"
helm template cert-manager jetstack/cert-manager --dry-run --include-crds --namespace cert-manager --debug --values="$dir_path/resources/cert-manager/values.yaml" >"$dir_path/resources/cert-manager/resources-helm.yaml"
helm template authelia authelia/authelia --dry-run --include-crds --namespace auth --debug --values="$dir_path/resources/authelia/values.yaml" >"$dir_path/resources/authelia/resources-helm.yaml"

helm template home-assistant k8s-at-home/home-assistant --dry-run --include-crds --namespace home-assistant --debug --values="$dir_path/resources/home-assistant/values.yaml" >"$dir_path/resources/home-assistant/resources-helm.yaml"
