#!/bin/sh

updateAll() {
    # ./mini-nomada/update.sh
    ./maxi-nomada/update.sh
    ./contabo-nomada/update.sh
    ./panda-nomada/update.sh
}

if [ -z "$1" ]; then
	echo "updating all"
    updateAll
elif [ "$1" = "helm" ]; then
	echo "updating helm"
    helm repo update
elif [ "$1" = "all" ]; then
	echo "updating helm and all"
    helm repo update
    updateAll
# elif [ "$1" = "mini" ]; then
# 	echo "updating mini"
#     ./mini-nomada/update.sh
elif [ "$1" = "maxi" ]; then
	echo "updating maxi"
    ./maxi-nomada/update.sh
elif [ "$1" = "panda" ]; then
	echo "updating panda"
    ./panda-nomada/update.sh
elif [ "$1" = "contabo" ]; then
	echo "updating contabo"
    ./contabo-nomada/update.sh
else
	echo "no matching do nothing"
fi
