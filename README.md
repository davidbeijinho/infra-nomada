# Infra Nomada
My gitops infrastructure

## Linting
npx mega-linter-runner@beta


# longhorn

pacman -S bash curl  grep awk open-iscsi
blkid 
lsblk
findmnt

```
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchFields:
          - key: metadata.name
            operator: In
            values:
                - mini-nomada
```
https://longhorn.io/kb/troubleshooting-rwx-mount-ownership-set-to-nobody/